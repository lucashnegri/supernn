/*
    This file is part of SuperNN.

    SuperNN is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SuperNN is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SuperNN.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (C) 2010 - 2015 Lucas Hermann Negri
*/

#include <locale>
#include <algorithm>
#include "data.hpp"

namespace SuperNN
{
SInfo::SInfo()
{
}

SInfo::~SInfo()
{
}

SInfo::SInfo(double _min, double _max) : min(_min), max(_max)
{
}

double SInfo::scale(const SInfo &to, double value) const
{
    const double brange = max - min;
    const double srange = to.max - to.min;

    if(brange < FSMALL || srange < FSMALL)
    {
        return (to.max + to.min) * 2; // mean
    }
    else
    {
        const double x = (value - min) / brange;
        return x * srange + to.min;
    }
}

Bounds::Bounds()
{
}

Bounds::~Bounds()
{
}

void Bounds::merge_with(const Bounds &other)
{
    for(unsigned n = 0, e = size(); n < e; ++n)
    {
        SInfo &i1 = at(n);
        const SInfo &i2 = other.at(n);

        i1.min = std::min(i1.min, i2.min);
        i1.max = std::max(i1.max, i2.max);
    }
}

void Bounds::save_file(std::ofstream &out) const
{
    out << size() << std::endl;
    out.precision(file_precision);

    for(unsigned b = 0, e = size(); b < e; ++b)
        out << at(b).min << "\t" << at(b).max << std::endl;
}

void Bounds::load_file(std::ifstream &inp)
{
    unsigned n;
    inp >> n;

    resize(n);

    for(unsigned i = 0; i < n; ++i)
        inp >> at(i).min >> at(i).max;
}

void Bounds::load_file(const std::string &path, Bounds &from, Bounds &to)
{
    std::ifstream inp;
    inp.open(path.c_str());

    if(!inp.is_open())
        throw Exception(ERROR_COULDNT_OPEN_FILE);

    inp.imbue(std::locale("C"));

    from.load_file(inp);
    to.load_file(inp);

    inp.close();
}

Data::Data()
{
}

Data::Data(unsigned rows, unsigned cols) : std::vector<Row>(rows, Row(cols)), n_total(cols)
{
    from.resize(cols);
    to.resize(cols);
}

Data::~Data()
{
}

Data::Data(unsigned rows, unsigned cols, const double st[])
    : std::vector<Row>(rows, Row(cols)), n_total(cols)
{
    from.resize(cols);
    to.resize(cols);

    for(unsigned r = 0; r < rows; ++r)
    {
        Row &a = at(r);

        for(unsigned c = 0; c < cols; ++c)
            a[c] = st[r * cols + c];
    }
}

Data::Data(unsigned rows, unsigned cols, const Row &row)
    : std::vector<Row>(rows, Row(cols)), n_total(cols)
{
    from.resize(cols);
    to.resize(cols);

    for(unsigned r = 0; r < rows; ++r)
    {
        Row &a = at(r);

        for(unsigned c = 0; c < cols; ++c)
            a[c] = row[r * cols + c];
    }
}

Data Data::drop_column(unsigned col) const
{
    if(col >= n_total)
        throw Exception(ERROR_INVALID_PARAMETERS);

    Data new_data;
    new_data.reserve(size());
    new_data.n_total = n_total - 1;

    /* bounds */
    for(unsigned i = 0; i < n_total; ++i)
    {
        if(i != col)
        {
            new_data.from.push_back(from[i]);
            new_data.to.push_back(to[i]);
        }
    }

    /* content */
    for(unsigned i = 0, e = size(); i < e; ++i)
    {
        Row r;

        for(unsigned j = 0; j < n_total; ++j)
        {
            if(j != col)
                r.push_back(at(i)[j]);
        }
        new_data.push_back(r);
    }

    return new_data;
}

void Data::shuffle()
{
    random_shuffle(begin(), end());
}

Data Data::sample(unsigned first, unsigned last) const
{
    const int n_rows = last - first;

    if(n_rows < 0)
        throw Exception(ERROR_INVALID_PARAMETERS);

    Data new_data(n_rows, n_total);

    for(int i = 0; i < n_rows; ++i)
        new_data[i] = at(first + i);

    new_data.from = from;
    new_data.to = to;

    return new_data;
}

Row &Data::add()
{
    push_back(Row(n_total));
    return back();
}

unsigned Data::load_file(const std::string &path)
{
    std::ifstream inp;
    inp.open(path.c_str());

    if(!inp.is_open())
        throw Exception(ERROR_COULDNT_OPEN_FILE);

    inp.imbue(std::locale("C"));

    unsigned n_rows;
    inp >> n_rows >> n_total;

    from.resize(n_total);
    to.resize(n_total);

    if(n_rows < 1 || n_total < 2)
    {
        inp.close();
        throw Exception(ERROR_INVALID_CONTENTS);
    }

    reserve(size() + n_rows);

    for(unsigned r = 0; r < n_rows; ++r)
    {
        Row row(n_total);

        for(unsigned i = 0; i < n_total; ++i)
            inp >> row[i];

        push_back(row);
    }

    inp.close();
    return n_rows;
}

void Data::save_file(const std::string &path) const
{
    std::ofstream out;
    out.open(path.c_str());

    if(!out.is_open())
        throw Exception(ERROR_COULDNT_OPEN_FILE);

    out.imbue(std::locale("C"));

    out << size() << " " << n_total << std::endl;
    out.precision(file_precision);

    for(unsigned r = 0, e = size(); r < e; ++r)
    {
        for(unsigned i = 0; i < n_total; ++i)
            out << at(r)[i] << " ";

        out << std::endl;
    }

    out.close();
}

void Data::save_info_file(const std::string &path) const
{
    std::ofstream out;
    out.open(path.c_str());

    if(!out.is_open())
        throw Exception(ERROR_COULDNT_OPEN_FILE);

    out.imbue(std::locale("C"));
    out.precision(file_precision);

    from.save_file(out);
    to.save_file(out);

    out.close();
}

Bounds &Data::calc_bounds()
{
    from.resize(n_total);

    for(unsigned n = 0; n < n_total; ++n)
    {
        double min = at(0)[n], max = at(0)[n];

        for(unsigned p = 1, e = size(); p < e; ++p)
        {
            const double v = at(p)[n];

            if(v < min)
                min = v;
            else if(v > max)
                max = v;
        }

        from[n] = SInfo(min, max);
    }

    return from;
}

void Data::scale_column(unsigned n, const SInfo &curv, const SInfo &newv)
{
    for(unsigned p = 0, e = size(); p < e; ++p)
        at(p)[n] = curv.scale(newv, at(p)[n]);
}

void Data::scale()
{
    for(unsigned c = 0; c < n_total; ++c)
        scale_column(c, from[c], to[c]);
}

void Data::scale(double min, double max)
{
    for(unsigned c = 0; c < n_total; ++c)
    {
        to[c] = SInfo(min, max);
        scale_column(c, from[c], to[c]);
    }
}

void Data::descale()
{
    for(unsigned c = 0; c < n_total; ++c)
        scale_column(c, to[c], from[c]);
}

void Data::k_fold(unsigned n, unsigned k, Data &p, Data &l) const
{
    for(unsigned r = 0, e = size(); r < e; ++r)
    {
        if(r % k == n)
            p.push_back(at(r));
        else
            l.push_back(at(r));
    }
}

}
