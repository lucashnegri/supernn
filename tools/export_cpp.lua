#!/usr/bin/env lua

--[[
    Reads a saved network and generates C++ code to run that network, without any
    runtime requirements.
    
    Usage: ./export.lua sample.net sample.info sample.cpp
--]]

local usage = string.format("Usage: %s sample.net sample.info sample.cpp", arg[0])
assert(arg[1] and arg[2] and arg[3], usage)

local f = assert( io.open(arg[1]), usage )

-- ** Read the network structure **
local layers        = {}
local n_layers      = f:read('*n')

for l = 1, n_layers do
    local layer     = {}
    local n_neurons = f:read('*n')
    
    for n = 1, n_neurons do
        local neuron = {}
        neuron.act   = f:read('*n')
        neuron.steep = f:read('*n')
        neuron.bias  = f:read('*n')
        local n_cons = f:read('*n')
        
        for c = 1, n_cons do
            local con = {}
            con.weigth    = f:read('*n')
            con.to_layer  = f:read('*n')
            con.to_neuron = f:read('*n')
            neuron[c] = con
        end
        
        layer[n] = neuron
    end
    
    layers[l] = layer
end

f:close()

-- generate the code
local ti = table.insert
local sf = string.format

-- header
local tmpl = [[
#include <vector>
#include <iostream>
#include <cmath>

class Bounds
{
public:
    Bounds(double _min, double _max) : min(_min), max(_max)
    {   
    }
    
    Bounds()
    {
    }
    
    double total() const
    {
        return max - min;
    }
    
    double scale(double value, const Bounds& to) const
    {
        value = (value - min) / total();
        return value * to.total() + to.min;
    }

private:
    double min, max;
};

class NeuralNetwork
{
public:
    NeuralNetwork() : mem(%d), from(%d), to(%d)
    {
%s
%s
    }
    
    std::vector<double> run(const std::vector<double>& inp)
    {
%s
    }

private:
    double sigmoid(double net, double steep) const
    {
        return 1.0 / (1.0 + std::exp(-2.0 * steep * net));
    }

    double sigmoid_sym(double net, double steep) const
    {
        return 2.0 / (1.0 + std::exp(-2.0 * steep * net)) - 1.0;
    }
    
    double linear(double net, double steep)
    {
        return steep * net;
    }
    
    std::vector<double> mem;
    std::vector<Bounds> from, to;
};

%s
]]

-- memory allocation
local total_neurons = 0
for _, layer in ipairs(layers) do
    total_neurons = total_neurons + #layer
end

-- inputs
local inp          = {'\t\t// Inputs (with scaling)'}
local idx_non_bias = 0
for i, neuron in ipairs(layers[1]) do
    if neuron.bias == 1 then
        ti(inp, sf('\t\tmem[%d] = 1;', i-1) )
    else
        ti(inp, sf('\t\tmem[%d] = from[%d].scale(inp[%d], to[%d]);', i-1, idx_non_bias, idx_non_bias, idx_non_bias) )
        idx_non_bias = idx_non_bias + 1
    end
end

for n = #layers[1], total_neurons-1 do
    ti(inp, sf('\t\tmem[%d] = 0;', n))
end

inp = table.concat(inp, '\n')

-- computation (hidden and ouput layers)

-- compute the histogram
local hist = {0}
for l = 1, #layers do
    hist[l+1] = hist[l] + #layers[l]
end

-- gets the neuron index by its layer and neuron indexes
function neuron_idx(layer, neuron)
    return hist[layer+1] + neuron
end

local ActFunc = {
    [0] = "sigmoid",
    [1] = "sigmoid_sym",
    [2] = "linear"
}

local comp = {'\n\t\t// Propagation\n'}

for l = 1, #layers do
    local layer = layers[l]
    ti(comp, sf('\t\t// layer %d', l))
    
    -- activation function
    if l > 1 then
        for n, neuron in ipairs(layer) do
            local idx     = neuron_idx(l-1, n-1)
            
            if neuron.bias == 0 then
                local actname = ActFunc[neuron.act]
                ti(comp, sf('\t\tmem[%d] = %s(mem[%d], %f);', idx, actname, idx, neuron.steep))
            else
                ti(comp, sf('\t\tmem[%d] = 1;', idx))
            end
        end
    end
    
    -- propagation
    for n, neuron in ipairs(layer) do
        local from_idx = neuron_idx(l-1, n-1)
        
        for c, conn in ipairs(neuron) do
            local to_idx   = neuron_idx(conn.to_layer, conn.to_neuron)
            ti(comp, sf('\t\tmem[%d] += %.16f * mem[%d];', to_idx, conn.weigth, from_idx) )
        end
    end
    
    ti(comp, '')
end
local comp = table.concat(comp, '\n')

-- output
local last   = #layers
local n_out  = #layers[last]
local out    = {}
ti(out, sf([[
        std::vector<double> out(%d);]], n_out))

for n = 0, n_out-1 do
    local idx = neuron_idx(last-1, n)
    ti(out, sf('\t\tout[%d] = to[%d].scale(mem[%d], from[%d]);', n, idx_non_bias+n, idx, idx_non_bias+n))
end

ti(out, '\n\t\treturn out;')
out = table.concat(out, '\n')

-- ** Read the scaling info **

local f = assert(io.open(arg[2]), usage)
local bfrom, bto = {}, {}

for _, tbl in ipairs{bfrom, bto} do
    local n = f:read('*n')
    for i = 1, n do
        local bounds = {}
        bounds.min = f:read('*n')
        bounds.max = f:read('*n')
        ti(tbl, bounds)
    end
end

assert(#bfrom == #bto, "Invalid bounds info file")

f:close()

-- total
local code = {}
ti(code, mem)
ti(code, inp)
ti(code, comp)
ti(code, out)
code  = table.concat(code, '\n')

local main = [[
int main()
{
    // just an example. Remove or adjust to your specific application
    NeuralNetwork net;
    std::vector<double> inp(2);
    
    inp[0] = -1;
    inp[1] = -1;
    std::cout << net.run(inp)[0] << std::endl;
    
    inp[0] =  1;
    inp[1] = -1;
    std::cout << net.run(inp)[0] << std::endl;
    
    inp[0] = -1;
    inp[1] =  1;
    std::cout << net.run(inp)[0] << std::endl;
    
    inp[0] =  1;
    inp[1] =  1;
    std::cout << net.run(inp)[0] << std::endl;
}
]]

-- bounds initialization code
local nscale = idx_non_bias + #layers[#layers]

local b1, b2 = {}, {}
for i = 1, #bfrom do
    ti(b1, sf('\t\tfrom[%d] = Bounds(%.16f, %.16f);', i-1, bfrom[i].min, bfrom[i].max))
    ti(b2, sf('\t\tto  [%d] = Bounds(%.16f, %.16f);', i-1,   bto[i].min, bto[i].max  ))
end
b1 = table.concat(b1, '\n')
b2 = table.concat(b2, '\n')

fcode = sf(tmpl, total_neurons, nscale, nscale, b1, b2, code, main):gsub('\t', '    ')

local outf = assert( io.open(arg[3], 'w'), usage )
outf:write(fcode)
outf:close()
