set(SuperNN_FOUND FALSE)
set(SuperNN_LIBRARY_NAME supernn)
set(SuperNN_INCLUDE_DIRS /usr/include /usr/local/include)

find_path(SuperNN_INCLUDE_DIR supernn ${SuperNN_INCLUDE_DIRS} )
find_library(SuperNN_LIBRARY NAMES ${SuperNN_LIBRARY_NAME} PATHS /usr/lib /usr/local/lib)

if(SuperNN_INCLUDE_DIR AND SuperNN_LIBRARY)
  set(SuperNN_FOUND TRUE)
endif()

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(SuperNN DEFAULT_MSG SuperNN_LIBRARY SuperNN_INCLUDE_DIR)
mark_as_advanced(SuperNN_INCLUDE_DIR SuperNN_LIBRARY)
