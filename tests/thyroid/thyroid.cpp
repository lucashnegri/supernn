#include <supernn>
#include <iostream>
#include "config.hpp"

using namespace SuperNN;
using namespace std;

int main()
{
    Utils::rand_seed();
    
    Network net = Network::make_mlp(21, 4, 3);
    net.set_activation(ACT_SIGMOID, 0.3);
    net.init_weights(-0.2, 0.2);
    
    IRprop t;
    t.prepare(net);
    
    Data data;
    data.load_file(TEST_DIR + "thyroid/thyroid.train");
    
    const unsigned e = t.train(net, data, 0.0, 1000);
    
    cout << "Training took     : " << e << " epochs" << endl;
    cout << "Training MSE      : " << net.calc_mse(data) << endl;
    cout << "Estimated test MSE: " << k_fold_error(t, net, data, 3, 0.0, 1000) << endl;

    data.clear();
    data.load_file(TEST_DIR + "thyroid/thyroid.test");

    cout << "\nActual test MSE   : " << net.calc_mse(data) << endl;
    cout << "Classification    : " << net.calc_class(data) << endl;
    
    return 0;
}
