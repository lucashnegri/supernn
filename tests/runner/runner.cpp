#include <supernn>
#include <iostream>
#include <cstdio> // for remove

#include "config.hpp"

using namespace SuperNN;
using namespace std;

int main()
{
    // training
    {
        Utils::rand_seed();
        
        Network net = Network::make_fcc(2, 4, 1);
        net.set_activation(ACT_SIGMOID_SYMMETRIC, 0.5);
        net.init_weights(-0.1, 0.1);

        NBN t;
        t.prepare(net);
        
        Data train;
        train.load_file(TEST_DIR + "runner/data.train");
        train.calc_bounds();
        train.scale(-1, 1);
        
        const unsigned e = t.train(net, train, 3e-5, 2000);
        
        cout << "Epochs        : " << e << endl;
        cout << "MSE           : " << net.calc_mse(train) << endl;
        cout << "Classification: " << net.calc_class(train, 0.01) << endl;
        
        net.save_file("out.net");
        train.save_info_file("out.info");
        cout << endl;
    }
    
    // to use the network from another program
    {
        Runner r("out.net", "out.info");
        
        r.inp(0) = 1;
        r.inp(1) = 3;
        r.run();
        cout << "Result 1: " << r.out(0) << endl;
        
        r.inp(0) = 4;
        r.inp(1) = 5;
        r.run();
        cout << "Result 2: " << r.out(0) << endl;
        
        r.inp(0) = 5;
        r.inp(1) = 5;
        r.run();
        cout << "Result 3: " << r.out(0) << endl;
        
        remove("out.net");
        remove("out.info");
    }
    
    return 0;
}
