# Description of the tests / examples

## approx

Trains an ANN to fit some data points that forms a line, with one outlier. Shows the difference between the
error metrics (mean squared error with the IRprop algorithm or mean absolute error with the IRpropL1 algorithm).

## iris

The famous IRIS dataset, obtained from [UCI][1].
Classification task with four attributes and three classes.

## runner

Shows how to use the Runner class to simplify the usage of a trained ANN, automatically handling the
data scaling.

## sanity

Sanity tests, used to check for bugs in the library. Should run without outputs and terminate naturaly without
aborting.

## sum

Simple example that shows the training of an ANN with linear activation functions by using the NBN
method.

## thyroid

A Thyroid database suited for training ANNs from [UCI][2].
Classification task with 3 classes.

## xor

Classical XOR problem. Defines the data inside the code and shows that only 2 hidden neurons are needed.

[1]: http://archive.ics.uci.edu/ml/datasets/Iris
[2]: http://archive.ics.uci.edu/ml/datasets/Thyroid+Disease
