#include <supernn>
#include <iostream>
#include <iomanip>
#include "config.hpp"

using namespace SuperNN;
using namespace std;

int main()
{
    Utils::rand_seed();
    
    /* 2 - 2 network, no bias. We can build the topology ourselves! */
    Network net;
    net.add_layers(2);
    Layer& input_layer  = net.layers[0];
    Layer& output_layer = net.layers[1];
    
    input_layer .add_neurons(2);
    output_layer.add_neurons(2);
    
    net.connect(0, 1);
    
    net.set_activation(ACT_LINEAR);
    
    NBN t;
    t.prepare(net);
    
    Data train, test;
    train.load_file(TEST_DIR + "sum/sum.train");
    test .load_file(TEST_DIR + "sum/sum.test");
    
    /* NBN should train in 1 epoch (just solve the linear system) */
    const unsigned e = t.train(net, train, 1e-8, 10);
    
    cout << "Epochs: " << e << endl;
    cout << "** Tests **" << endl;
    cout << "\nMSE: " << net.calc_mse(test) << endl;
    
    cout << setprecision(5) << endl;

    for(unsigned i = 0, e = test.size(); i < e; ++i)
    {
        const Row& res = net.run(test[i]);
        cout << test[i][0] << " {+,-} " << test[i][1] << " == " << res[0] << " " << res[1] << endl;
    }
    
    return 0;
}
