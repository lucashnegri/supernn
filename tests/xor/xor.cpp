/**
 * Classic XOR problem.
 */

#include <supernn>
#include <iostream>
#include "config.hpp"

using namespace SuperNN;
using namespace std;

int main()
{
    /** Initialize the RNG */
    Utils::rand_seed();
    
    /** Create the network */
    Network net = Network::make_fcc(2, 2, 1);
    net.set_activation(ACT_SIGMOID_SYMMETRIC, 0.5);
    net.init_weights(-0.2, 0.2);

    /** Create the trainer and prepare the network */
    NBN t;
    t.prepare(net);
    
    /** Create the training data */
    const double data[] = {
        -1.0, -1.0, -1,
        -1.0, +1.0, +1,
        +1.0, -1.0, +1,
        +1.0, +1.0, -1
    };
    const Data full(4, 3, data);
    
    /** Train the network */
    const unsigned e = t.train(net, full, 1e-5, 50);
    
    /** Print some statistics */
    cout << "Epochs        : " << e << endl;
    cout << "Classification: " << net.calc_class(full) << endl;
    
    return 0;
}
