/*
        This file is part of SuperNN.

        SuperNN is free software: you can redistribute it and/or modify
        it under the terms of the GNU Lesser General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        SuperNN is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU Lesser General Public License for more details.

        You should have received a copy of the GNU Lesser General Public License
        along with SuperNN.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (C) 2010 - 2015 Lucas Hermann Negri
*/

#include <cstdlib>
#include <cstdio>

#ifndef _WIN32
#include <sys/time.h>
#else
#include <ctime>
#endif
#include "utils.hpp"

namespace SuperNN
{

namespace Utils
{

double rand_double(double max)
{
    return rand() / (RAND_MAX / max);
}

void rand_seed()
{
    unsigned seed;

#ifndef _WIN32
    FILE *rf = fopen("/dev/urandom", "r");

    if(!rf)
    {
        struct timeval tv;
        gettimeofday(&tv, NULL);
        seed = tv.tv_usec + tv.tv_sec;
    }
    else
    {
        fread(&seed, sizeof(seed), 1, rf);
        fclose(rf);
    }

#else
    seed = (unsigned)time(NULL);
#endif
    srand(seed);
}

}
}
