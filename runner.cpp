/*
        This file is part of SuperNN.

        SuperNN is free software: you can redistribute it and/or modify
        it under the terms of the GNU Lesser General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        SuperNN is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU Lesser General Public License for more details.

        You should have received a copy of the GNU Lesser General Public License
        along with SuperNN.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (C) 2010 - 2015 Lucas Hermann Negri
*/

#include "neuron.hpp"
#include "runner.hpp"

namespace SuperNN
{

Runner::Runner()
{
}

Runner::Runner(const std::string &net_path, const std::string &info_path)
{
    net.load_file(net_path);
    load_info_file(info_path);
}

Runner::~Runner()
{
}

void Runner::set_bounds(Bounds &from, Bounds &to)
{
    const unsigned n_out = net[net.size() - 1].size();
    unsigned n_inp = from.size() - n_out; // not so smart foreach!

    inp_data = Data(1, n_inp);
    out_data = Data(1, n_out);

    inp_data.add();
    out_data.add();

    // input neurons
    for(unsigned i = 0; i < n_inp; ++i)
    {
        inp_data.from[i] = from[i];
        inp_data.to[i] = to[i];
    }

    // output neurons
    for(unsigned i = n_inp, e = from.size(); i < e; ++i)
    {
        unsigned idx = i - n_inp;
        out_data.from[idx] = from[i];
        out_data.to[idx] = to[i];
    }
}

void Runner::load_info_file(const std::string &info_path)
{
    Bounds from, to;
    Bounds::load_file(info_path, from, to);
    set_bounds(from, to);
}

double &Runner::inp(unsigned idx)
{
    return inp_data[0][idx];
}

double Runner::out(unsigned idx)
{
    return out_data[0][idx];
}

void Runner::run()
{
    inp_data.scale();
    out_data[0] = net.run(inp_data[0]);
    out_data.descale();
}

}
