/*
    This file is part of SuperNN.

    SuperNN is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SuperNN is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SuperNN.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (C) 2010 - 2015 Lucas Hermann Negri
*/

#include <locale>
#include <fstream>
#include "activation.hpp"
#include "utils.hpp"
#include "neuron.hpp"
#include "network.hpp"

namespace SuperNN
{

Layer::Layer()
{
}

Layer::~Layer()
{
}

void Layer::add_neuron(Neuron &n)
{
    push_back(n);
}

void Layer::add_neurons(unsigned n_neurons, bool bias)
{
    reserve(size() + n_neurons);

    for(unsigned n = 0; n < n_neurons; ++n)
    {
        Neuron l(bias);
        add_neuron(l);
    }
}

void Layer::set_activation(ActFuncType type, double s)
{
    for(unsigned n = 0, e = size(); n < e; ++n)
        at(n).set_activation(type, s);
}

void Layer::connect(unsigned to_layer, unsigned to_neuron)
{
    for(unsigned n = 0, e = size(); n < e; ++n)
        at(n).connect(to_layer, to_neuron);
}

void Network::add_layer(Layer &l)
{
    layers.push_back(l);
}

void Network::add_layers(unsigned n_layers)
{
    layers.reserve(layers.size() + n_layers);

    for(unsigned l = 0; l < n_layers; ++l)
    {
        Layer nl;
        add_layer(nl);
    }
}

Network::Network() : n_input(0)
{
}

Network::~Network()
{
}

Network Network::make_mlp(unsigned input, unsigned hidden, unsigned output)
{
    Network net;
    net.add_layers(3);

    Layer &input_layer = net.layers[0];
    Layer &hidden_layer = net.layers[1];
    Layer &output_layer = net.layers[2];

    input_layer.add_neurons(input);
    input_layer.add_neurons(1, true);
    hidden_layer.add_neurons(hidden);
    output_layer.add_neurons(output);

    net.connect(0, 1);
    net.connect_neuron_to_layer(0, input, 2);
    net.connect(1, 2);

    return net;
}

Network Network::make_mlp(unsigned input, unsigned hidden1, unsigned hidden2, unsigned output)
{
    Network net;
    net.add_layers(4);

    Layer &input_layer = net.layers[0];
    Layer &hidden1_layer = net.layers[1];
    Layer &hidden2_layer = net.layers[2];
    Layer &output_layer = net.layers[3];

    input_layer.add_neurons(input);
    input_layer.add_neurons(1, true);
    hidden1_layer.add_neurons(hidden1);
    hidden2_layer.add_neurons(hidden2);
    output_layer.add_neurons(output);

    net.connect(0, 1);

    net.connect_neuron_to_layer(0, input, 2);
    net.connect(1, 2);

    net.connect_neuron_to_layer(0, input, 3);
    net.connect(2, 3);

    return net;
}

Network Network::make_mlp(unsigned input, unsigned output)
{
    Network net;
    net.add_layers(2);

    Layer &input_layer = net.layers[0];
    Layer &output_layer = net.layers[1];

    input_layer.add_neurons(input);
    input_layer.add_neurons(1, true);
    output_layer.add_neurons(output);

    net.connect(0, 1);

    return net;
}

Network Network::make_fcc(unsigned input, unsigned hidden, unsigned output)
{
    Network net;
    net.add_layers(hidden + 2);

    net.layers[0].add_neurons(input);
    net.layers[0].add_neurons(1, true);

    for(unsigned l = 1; l <= hidden; ++l)
        net.layers[l].add_neurons(1);

    unsigned last = net.layers.size() - 1;
        net.layers[last].add_neurons(output);

    for(unsigned l1 = 0; l1 < last; ++l1)
        for(unsigned l2 = l1+1; l2 <= last; ++l2)
            net.connect(l1, l2);

    return net;
}

const Row &Network::run(const Row &in, bool calc_error)
{
    const unsigned last_layer = layers.size() - 1;
    const unsigned in_size    = in.size();
    clear_neurons(true, true);

    /* copies the inputs to the first neuron layer and count the non-biases */
    n_input = 0;

    for(unsigned i = 0, e = layers[0].size(); i < e; ++i)
    {
        if(!layers[0][i].bias)
        {
            if(n_input >= in_size)
                throw Exception(ERROR_DIMENSIONS_DO_NOT_MATCH);
            else
                layers[0][i].out = in[n_input++];
        }
    }

    /* propagate the signal */
    for(unsigned l = 0; l < last_layer; ++l)
    {
        for(unsigned n = 0, e = layers[l].size(); n < e; ++n)
        {
            Neuron &neuron = layers[l][n];

            for(unsigned c = 0, e = neuron.size(); c < e; ++c)
            {
                Connection &conn = neuron[c];
                double v = neuron.out * conn.weight;
                layers[conn.to_layer][conn.to_neuron].net += v;
            }
        }

        for(unsigned n = 0, e = layers[l + 1].size(); n < e; ++n)
        {
            Neuron &neuron = layers[l + 1][n];
            neuron.out = ActFunc::activation(neuron);
        }
    }

    const unsigned n_out = layers[last_layer].size();
    last_output.resize(n_out);

    for(unsigned n = 0; n < n_out; ++n)
    {
        Neuron &neuron = layers[last_layer][n];
        last_output[n] = neuron.out;

        if(calc_error)
            neuron.err = in[n_input + n] - neuron.out;
    }

    return last_output;
}

double Network::calc_mse(const Data &data)
{
    const unsigned n_out = layers[layers.size() - 1].size();
    double mse = 0;

    for(unsigned p = 0, e = data.size(); p < e; ++p)
    {
        const Row& r = run(data[p], true);

        for(unsigned n = 0; n < n_out; ++n)
        {
            double err = data[p][n_input + n] - r[n];
            mse += err * err;
        }
    }

    mse /= data.size();

    return mse;
}

double Network::calc_mae(const Data &data)
{
    const unsigned n_out = layers[layers.size() - 1].size();
    double mae = 0;

    for(unsigned p = 0, e = data.size(); p < e; ++p)
    {
        const Row& r = run(data[p], true);

        for(unsigned n = 0; n < n_out; ++n)
        {
            double err = data[p][n_input + n] - r[n];
            mae += std::abs(err);
        }
    }

    mae /= data.size();

    return mae;
}

double Network::calc_class(const Data &data, double limit)
{
    unsigned nt = 0;

    for(unsigned p = 0, e = data.size(); p < e; ++p)
    {
        run(data[p]);
        const unsigned last = layers.size() - 1;
        unsigned s = 0;
        const unsigned n_output = layers[last].size();

        for(unsigned n = 0; n < n_output; ++n)
        {
            if(std::abs(layers[last][n].out - data[p][n_input + n]) < limit)
                ++s;
        }

        if(s == n_output)
            ++nt;
    }

    return nt * 100 / (double)data.size();
}

double Network::calc_class_higher(const Data &data)
{
    unsigned nt = 0;

    for(unsigned p = 0, e = data.size(); p < e; ++p)
    {
        run(data[p]);
        const unsigned last = layers.size() - 1;
        const unsigned n_output = layers[last].size();
        unsigned max1 = -1, max2 = -1;
        double val1 = -1e30, val2 = -1e30;

        for(unsigned n = 0; n < n_output; ++n)
        {
            if(layers[last][n].out > val1)
                val1 = layers[last][n].out, max1 = n;

            if(data[p][n_input + n] > val2)
                val2 = data[p][n_input + n], max2 = n;
        }

        if(max1 == max2)
            ++nt;
    }

    return nt * 100 / (double)data.size();
}

void Network::init_weights(double min, double max)
{
   for(unsigned l = 0, e = layers.size(); l < e; ++l)
    {
        for(unsigned n = 0, e = layers[l].size(); n < e; ++n)
        {
            Neuron &neuron = layers[l][n];

            for(unsigned c = 0, e = neuron.size(); c < e; ++c)
                neuron[c].weight = Utils::rand_double(max - min) + min;
        }
    }
}

void Network::set_activation(ActFuncType type, double s)
{
    for(unsigned l = 0, e = layers.size(); l < e; ++l)
        layers[l].set_activation(type, s);
}

void Network::save_file(const std::string &path) const
{
    std::ofstream out;
    out.open(path.c_str());

    if(!out.is_open())
        throw Exception(ERROR_COULDNT_OPEN_FILE);

    out.imbue(std::locale("C"));

    out << layers.size() << std::endl;
    out.precision(file_precision);

    for(unsigned l = 0, e = layers.size(); l < e; ++l)
    {
        out << layers[l].size() << std::endl;

        for(unsigned n = 0, e = layers[l].size(); n < e; ++n)
        {
            const Neuron &neuron = layers[l][n];
            out << neuron.act_func << " " << neuron.steep << " " << neuron.bias << std::endl;
            out << neuron.size() << std::endl;

            for(unsigned c = 0, e = neuron.size(); c < e; ++c)
            {
                const Connection &conn = neuron[c];
                out << conn.weight << " " << conn.to_layer << " " << conn.to_neuron << std::endl;
            }
        }
    }

    out.close();
}

void Network::load_file(const std::string &path)
{
    layers.clear();
    last_output.clear();
    n_input = 0;

    std::ifstream inp;
    inp.open(path.c_str());

    if(!inp.is_open())
        throw Exception(ERROR_COULDNT_OPEN_FILE);

    inp.imbue(std::locale("C"));

    int n_layers;
    inp >> n_layers;

    if(n_layers < 1)
    {
        inp.close();
        throw Exception(ERROR_INVALID_CONTENTS);
    }

    layers.resize(n_layers);

    for(int l = 0; l < n_layers; ++l)
    {
        int n_neurons;
        inp >> n_neurons;

        if(n_neurons < 1)
        {
            inp.close();
            throw Exception(ERROR_INVALID_CONTENTS);
        }

        layers[l].resize(n_neurons);

        for(int n = 0; n < n_neurons; ++n)
        {
            Neuron &neuron = layers[l][n];
            int act, bias; // read as int
            inp >> act >> neuron.steep >> bias;
            neuron.act_func = (ActFuncType)act;
            neuron.bias = (bool)bias; // ignore VC performance warning

            int n_conns;
            inp >> n_conns;

            if(n_conns < 0)
            {
                inp.close();
                throw Exception(ERROR_INVALID_CONTENTS);
            }

            neuron.conns.resize(n_conns);

            for(int c = 0; c < n_conns; ++c)
            {
                Connection &conn = neuron[c];
                inp >> conn.weight >> conn.to_layer >> conn.to_neuron;
            }
        }
    }

    inp.close();
}

void Network::connect(unsigned from_layer, unsigned to_layer)
{
    for(unsigned n = 0, e = layers[to_layer].size(); n < e; ++n)
        layers[from_layer].connect(to_layer, n);
}

void Network::connect_neuron_to_layer(unsigned from_layer, unsigned from_neuron, unsigned to_layer)
{
    Neuron &neuron = layers[from_layer][from_neuron];

    for(unsigned n = 0, e = layers[to_layer].size(); n < e; ++n)
        neuron.connect(to_layer, n);
}

void Network::clear_neurons(bool clear_delta, bool clear_run)
{
   for(unsigned l = 0, e = layers.size(); l < e; ++l)
    {
        for(unsigned n = 0, e = layers[l].size(); n < e; ++n)
        {
            Neuron &neuron = layers[l][n];

            if(clear_delta)
            {
                neuron.delta = 0;
                neuron.delta_ok = false;
            }

            if(clear_run)
            {
                neuron.out = neuron.bias ? 1 : 0;
                neuron.net = 0;
                neuron.err = 0;
            }
        }
    }
}

unsigned Network::calc_num_weights() const
{
    unsigned n_weights = 0;

    for(unsigned l = 0, e = layers.size(); l < e; ++l)
    {
        const Layer &layer = layers[l];

        for(unsigned n = 0, e = layer.size(); n < e; ++n)
            n_weights += layer[n].size();
    }

    return n_weights;
}

unsigned Network::calc_num_neurons() const
{
    unsigned n_neurons = 0;

    for(unsigned l = 0, e = layers.size(); l < e; ++l)
        n_neurons += layers[l].size();

    return n_neurons;
}

unsigned Network::calc_num_inputs() const
{
    unsigned n_inputs = 0;

    for(unsigned i = 0, e = layers[0].size(); i < e; ++i)
    {
        if(!layers[0][i].bias)
            ++n_inputs;
    }

    return n_inputs;
}

const Layer &Network::operator[](unsigned l) const
{
    return layers[l];
}

Layer &Network::operator[](unsigned l)
{
    return layers[l];
}

unsigned Network::size() const
{
    return layers.size();
}

}
