/*
    This file is part of SuperNN.

    SuperNN is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SuperNN is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SuperNN.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (C) 2010 - 2015 Lucas Hermann Negri
*/

#include <vector>
#include <Eigen/Dense>

#include "activation.hpp"
#include "neuron.hpp"
#include "utils.hpp"
#include "data.hpp"
#include "network.hpp"
#include "training.hpp"

namespace SuperNN
{

double TrainingAlgorithm::delta(Network &net, unsigned l, unsigned n)
{
    std::vector<Layer> &layers = net.layers;
    Neuron &neuron = layers[l][n];

    if(!neuron.delta_ok)
    {
        neuron.delta_ok = true;

        if(l == layers.size() - 1)
        {
            const double gd = ActFunc::derivative(neuron);
            neuron.delta = neuron.err * gd;
        }
        else
        {
            for(unsigned c = 0, e = neuron.size(); c < e; ++c)
            {
                const Connection &conn = neuron[c];
                const double d = delta(net, conn.to_layer, conn.to_neuron);
                const double gd = ActFunc::derivative(neuron);
                neuron.delta += d * conn.weight * gd;
            }
        }
    }

    return neuron.delta;
}

TrainingAlgorithm::~TrainingAlgorithm()
{

}

void TrainingAlgorithm::prepare(Network &net)
{
    (void)net; // stop the warning
}

void TrainingAlgorithm::check(const Network& net, const Data& data) const
{
    if(data.empty())
        throw Exception(ERROR_INVALID_PARAMETERS);

    const unsigned total_size = net.calc_num_inputs() + net.layers.back().size();

    if(total_size != data[0].size())
        throw Exception(ERROR_DIMENSIONS_DO_NOT_MATCH);
}

void TrainingAlgorithm::derror_acc(Network &net)
{
    std::vector<Layer> &layers = net.layers;

    for(unsigned l = 0, e = layers.size(); l < e; ++l)
    {
        for(unsigned n = 0, e = layers[l].size(); n < e; ++n)
        {
            Neuron &neuron = layers[l][n];
            for(unsigned c = 0, e = neuron.size(); c < e; ++c)
            {
                Connection &conn = neuron[c];
                Neuron &to = layers[conn.to_layer][conn.to_neuron];
                conn.derror += -to.delta * neuron.out;
            }
        }
    }
}

void TrainingAlgorithm::clear_derror_acc(Network &net)
{
    for(unsigned l = 0, e = net.layers.size(); l < e; ++l)
    {
        for(unsigned n = 0, e = net.layers[l].size(); n < e; ++n)
        {
            Neuron &neuron = net.layers[l][n];

            for(unsigned c = 0, e = neuron.size(); c < e; ++c)
            neuron[c].derror = 0;
        }
    }
}

ImplBackprop::ImplBackprop() : eta_df(0.95), eta_if(1.03), eta_min(1e-5), eta_max(1e5), eta(0.7)
{
}

ImplBackprop::~ImplBackprop()
{
}

void ImplBackprop::update_weights(Network &net, double factor)
{
    std::vector<Layer> &layers = net.layers;
    const double eta_f = eta * factor;

    for(unsigned l = 0, e = layers.size(); l < e; ++l)
    {
        for(unsigned n = 0, e = layers[l].size(); n < e; ++n)
        {
            Neuron &neuron = layers[l][n];

            for(unsigned c = 0, e = neuron.size(); c < e; ++c)
            {
                Connection &conn = neuron[c];
                conn.weight += -eta_f * conn.derror;
            }
        }
    }
}

void ImplBackprop::update_eta(double mse, double last_mse)
{
    if(mse >= last_mse)
    {
        eta *= eta_df;

        if(eta < eta_min)
            eta = eta_min;
    }
    else
    {
        eta *= eta_if;

        if(eta > eta_max)
            eta = eta_max;
    }
}

Incremental::Incremental()
{
}

Incremental::~Incremental()
{
}

unsigned Incremental::train(Network &net, const Data &data, double dmse, unsigned max_epochs)
{
    check(net, data);
    std::vector<Layer> &layers = net.layers;

    double last_mse = -1;
    unsigned e;

    for(e = 1; e <= max_epochs; ++e)
    {
        for(unsigned p = 0, e = data.size(); p < e; ++p)
        {
            clear_derror_acc(net);
            net.run(data[p], true);

            for(unsigned n = 0, e = layers[1].size(); n < e; ++n)
                delta(net, 1, n);

            derror_acc(net);
            update_weights(net);
        }

        const double mse = net.calc_mse(data);
        update_eta(mse, last_mse);

        if(mse <= dmse) break;

        last_mse = mse;
    }

    return e;
}

Batch::Batch()
{
}

Batch::~Batch()
{
}

unsigned Batch::train(Network &net, const Data &data, double dmse, unsigned max_epochs)
{
    check(net, data);
    std::vector<Layer> &layers = net.layers;

    double last_mse = -1, isize = data.size();
    unsigned e;

    for(e = 1; e <= max_epochs; ++e)
    {
        clear_derror_acc(net);

        for(unsigned p = 0, e = data.size(); p < e; ++p)
        {
            net.run(data[p], true);

            for(unsigned n = 0, e = layers[1].size(); n < e; ++n)
                delta(net, 1, n);

            derror_acc(net);
        }

        update_weights(net, isize);

        const double mse = net.calc_mse(data);
        update_eta(mse, last_mse);

        if(mse <= dmse) break;

        last_mse = mse;
    }

    return e;
}

/* IRprop algorithm */

IRprop::IRprop() : delta_df(0.5), delta_if(1.2), delta_min(0), delta_max(50), delta_zero(0.1)
{
}

IRprop::~IRprop()
{
}

void IRprop::prepare(Network &net)
{
    std::vector<Layer> &layers = net.layers;

    for(unsigned l = 0, e = layers.size(); l < e; ++l)
    {
        for(unsigned n = 0, e = layers[l].size(); n < e; ++n)
        {
            std::vector<Connection> &conns = layers[l][n].conns;

            for(unsigned c = 0, e = conns.size(); c < e; ++c)
            {
                conns[c].aux1 = 0;          // last local error gradient
                conns[c].aux2 = delta_zero; // last weight change
            }
        }
    }
}

void IRprop::update_weights(Network &net)
{
    std::vector<Layer> &layers = net.layers;

    for(unsigned l = 0, e = layers.size(); l < e; ++l)
    {
        for(unsigned n = 0, e = layers[l].size(); n < e; ++n)
        {
            Neuron &neuron = layers[l][n];
            for(unsigned c = 0, e = neuron.size(); c < e; ++c)
            {
                Connection &conn = neuron[c];
                const double sign = conn.aux1 * conn.derror;

                if(sign > 0)
                {
                    conn.aux2 *= delta_if;
                }
                else
                {
                    if(sign < 0)
                    {
                        conn.aux2 *= delta_df;
                        conn.derror = 0;
                    }
                }

                conn.aux2 = Utils::limit(delta_min, delta_max, conn.aux2);

                if(conn.derror > 0)
                    conn.weight -= conn.aux2;
                else if(conn.derror < 0)
                    conn.weight += conn.aux2;

                conn.aux1 = conn.derror;
            }
        }
    }
}

unsigned IRprop::train(Network &net, const Data &data, double dmse, unsigned max_epochs)
{
    check(net, data);
    std::vector<Layer> &layers = net.layers;
    unsigned e;

    for(e = 1; e <= max_epochs; ++e)
    {
        clear_derror_acc(net);

        for(unsigned p = 0, e = data.size(); p < e; ++p)
        {
            net.run(data[p], true);

            for(unsigned n = 0, e = layers[1].size(); n < e; ++n)
                delta(net, 1, n);

            derror_acc(net);
        }

        update_weights(net);

        if(dmse > 0 && net.calc_mse(data) <= dmse)
            break;
    }

    return e;
}

/* IRprop L1 minimization algorithm */
IRpropL1::IRpropL1() : IRprop()
{
}

IRpropL1::~IRpropL1()
{
}

double IRpropL1::delta(Network &net, unsigned l, unsigned n)
{
    std::vector<Layer> &layers = net.layers;

    Neuron &neuron = layers[l][n];

    if(!neuron.delta_ok)
    {
        neuron.delta_ok = true;

        if(l == layers.size() - 1)
        {
            const double gd = ActFunc::derivative(neuron);
            neuron.delta = neuron.err > 0 ? gd : (neuron.err < 0 ? -gd : 0);
        }
        else
        {
            for(unsigned c = 0, e = neuron.size(); c < e; ++c)
            {
                Connection &conn = neuron[c];
                const double d = delta(net, conn.to_layer, conn.to_neuron);
                const double gd = ActFunc::derivative(neuron);
                neuron.delta += d * conn.weight * gd;
            }
        }
    }

    return neuron.delta;
}

/* NBN algorithm */

/* private members of NBN algorithm */
struct NBN::Hist
{
    /** Quasi-Hessian matrix */
    Eigen::MatrixXd hessian;

    /** Transposed Jacobian line */
    Eigen::VectorXd t_jacob_line;

    /** Error gradient matrix */
    Eigen::VectorXd gradient;

    /** Weight matrix (same as the network weights, but in a suitable
     * format to the algorithm */
    Eigen::VectorXd weights;

    /** Backup of the weights, for backtracking */
    Eigen::VectorXd weights_backup;
};

NBN::NBN() : mu_zero(0.1), mu(mu_zero), mu_min(1e-50), mu_max(1e50), beta(2.0), local_iters(5),
    n_weights(0), h(new NBN::Hist()) 
{
}

NBN::~NBN()
{
    delete h;
}

void NBN::prepare(Network &net)
{
    n_weights = net.calc_num_weights();

    h->hessian = Eigen::MatrixXd(n_weights, n_weights);
    h->gradient = Eigen::VectorXd(n_weights);
    h->weights = Eigen::VectorXd(n_weights);
    h->t_jacob_line = Eigen::VectorXd(n_weights);
    mu = mu_zero;
}

void NBN::get_weights(const Network &net)
{
    unsigned i = 0;

    for(unsigned l = 0, e = net.layers.size(); l < e; ++l)
    {
        const Layer &layer = net.layers[l];

        for(unsigned n = 0, e = layer.size(); n < e; ++n)
        {
            for(unsigned c = 0, e = layer[n].size(); c < e; ++c)
            {
                h->weights(i) = layer[n][c].weight;
                ++i;
            }
        }
    }
}

void NBN::set_weights(Network &net) const
{
    unsigned i = 0;

    for(unsigned l = 0, e = net.layers.size(); l < e; ++l)
    {
        Layer &layer = net.layers[l];

        for(unsigned n = 0, e = layer.size(); n < e; ++n)
        {
            for(unsigned c = 0, e = layer[n].size(); c < e; ++c)
            {
                layer[n][c].weight = h->weights(i);
                ++i;
            }
        }
    }
}

double NBN::delta(Network &net, unsigned l, unsigned n, unsigned m)
{
    std::vector<Layer> &layers = net.layers;
    Neuron &neuron = layers[l][n];

    if(!neuron.delta_ok)
    {
        neuron.delta_ok = true;

        if(l == layers.size() - 1)
        {
            if(n == m)
                neuron.delta = ActFunc::derivative(neuron);
        }
        else
        {
            for(unsigned c = 0, e = neuron.size(); c < e; ++c)
            {
                const Connection &conn = neuron[c];
                const double d = delta(net, conn.to_layer, conn.to_neuron, m);
                const double gd = ActFunc::derivative(neuron);
                neuron.delta += d * conn.weight * gd;
            }
        }
    }

    return neuron.delta;
}

void NBN::calc_jacobian_transp_line(Network &net, unsigned m)
{
    std::vector<Layer> &layers = net.layers;

    net.clear_neurons(true, false);
    clear_derror_acc(net);

    for(unsigned n = 0, e = layers[1].size(); n < e; ++n)
    delta(net, 1, n, m);

    derror_acc(net);

    unsigned i = 0;

    for(unsigned l = 0, e = layers.size(); l < e; ++l)
    {
        const Layer &layer = layers[l];

        for(unsigned n = 0, e = layer.size(); n < e; ++n)
        {
            for(unsigned c = 0, e = layer[n].size(); c < e; ++c)
            {
                h->t_jacob_line(i) = -layer[n][c].derror;
                ++i;
            }
        }
    }
}

void NBN::update_hessian_gradient(double err)
{
    h->hessian.noalias() += h->t_jacob_line * h->t_jacob_line.transpose();
    h->gradient.noalias() += h->t_jacob_line * err;
}

void NBN::update_weights(Network &net)
{
    const unsigned n = n_weights;
    h->weights.noalias() +=
        (h->hessian + Eigen::MatrixXd::Identity(n, n) * mu).colPivHouseholderQr().solve(
            h->gradient);
    set_weights(net);
}

bool NBN::update_mu(double mse, double last_mse)
{
    bool res = false;

    if(mse >= last_mse)
    {
        h->weights = h->weights_backup;
        mu *= beta;
        res = false;
    }
    else
    {
        mu /= beta;
        h->weights_backup = h->weights;
        res = true;
    }

    mu = Utils::limit(mu_min, mu_max, mu);
    return res;
}

unsigned NBN::train(Network &net, const Data &data, double dmse, unsigned max_epochs)
{
    check(net, data);
    Layer &last = net.layers.back();
    double last_mse = net.calc_mse(data);
    unsigned e;

    get_weights(net);
    h->weights_backup = h->weights;

    for(e = 1; e <= max_epochs; ++e)
    {
        h->hessian.setZero();
        h->gradient.setZero();

        for(unsigned p = 0, e = data.size(); p < e; ++p)
        {
            net.run(data[p], true);

            for(unsigned m = 0, e = last.size(); m < e; ++m)
            {
                calc_jacobian_transp_line(net, m);
                update_hessian_gradient(last[m].err);
            }
        }

        double mse = -1;

        /* local iterations */
        for(unsigned tt = 0; tt < local_iters; ++tt)
        {
            update_weights(net);
            mse = net.calc_mse(data);

            if(update_mu(mse, last_mse))
                break;
        }

        if(mse <= dmse)
            break;

        last_mse = mse;
    }

    return e;
}

double k_fold_error(TrainingAlgorithm& algo, const Network &net, const Data &data, unsigned k,
                    double dmse, unsigned max_epochs)
{
    double acc = 0;

    for(unsigned n = 0; n < k; ++n)
    {
        Network cur_net   = net;

        Data validation, training;
        data.k_fold(n, k, validation, training);

        algo.prepare(cur_net);
        algo.train(cur_net, training, dmse, max_epochs);

        acc += cur_net.calc_mse(validation);
    }

    return acc / k;
}

unsigned early_stopping(TrainingAlgorithm &algo, Network &net, const Data& training, const Data& validation, unsigned step_size, unsigned max_stuck, unsigned max_epochs)
{
    algo.prepare(net);
    Network best_net = net;
    double best_mse  = net.calc_mse(validation);
    unsigned best_it = 0;

    unsigned stuck = 0, it = 0;

    while( (stuck < max_stuck) && (it < max_epochs) )
    {
        algo.train(net, training, -1, step_size);
        it += step_size;

        const double new_mse = net.calc_mse(validation);
        if(new_mse < best_mse)
        {
            best_net = net;
            best_mse = new_mse;
            best_it = it;
            stuck = 0;
        }
        else
        {
            stuck += step_size;
        }
    }

    net = best_net;
    return best_it;
}

}
