/*
        This file is part of SuperNN.

        SuperNN is free software: you can redistribute it and/or modify
        it under the terms of the GNU Lesser General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        SuperNN is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU Lesser General Public License for more details.

        You should have received a copy of the GNU Lesser General Public License
        along with SuperNN.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (C) 2010 - 2015 Lucas Hermann Negri
*/

#ifndef SUPERNN_UTILS_HPP
#define SUPERNN_UTILS_HPP

#include <exception>
#include <algorithm>

#if defined (WIN32)
  #if defined (_MSC_VER)
    #pragma warning(disable: 4251)
  #endif
  
  #define SUPERNN_EXPORT __declspec(dllexport)
#else
  #define SUPERNN_EXPORT
#endif

namespace SuperNN
{
/** Precision used when writting floating point number to files */
const unsigned file_precision = 12;

/** Errors that the library can throw */
enum ErrorType
{
    /** thrown when a file couldn't be opened */
    ERROR_COULDNT_OPEN_FILE,

    /** thrown when a file has invalid contents */
    ERROR_INVALID_CONTENTS,

    /** thrown when calling a function with invalid parameters */
    ERROR_INVALID_PARAMETERS,

    /** thrown when a matrix can't be solved/inverted */
    ERROR_SINGULARITY,

    /** thrown when training with a non-diferentiable activation function */
    ERROR_NOT_DIFERENTIABLE,

    /** thrown when the dimensions of a Row and the network does not match */
    ERROR_DIMENSIONS_DO_NOT_MATCH,

    /** thrown when no other type applies */
    ERROR_GENERIC
};

/**
 * The exception can be identified by the type() method.
 */
class SUPERNN_EXPORT Exception : public std::exception
{

public:
    Exception(ErrorType type = ERROR_GENERIC) : m_type(type)
    {
    }

    const char* what() const throw()
    {
        return "SuperNN exception";
    }

    ErrorType type()
    {
        return m_type;
    }

private:
    ErrorType m_type;
};

/**
 * Functions used in other sections.
 */
namespace Utils
{
/**
 * Returns a pseudo-random double.
 *
 * @param max Maximum value
 * @return Pseudo-random double in the range [0, max]
 */
SUPERNN_EXPORT double rand_double(double max);

/**
 * Initializes the random number generator.
 */
SUPERNN_EXPORT void rand_seed();

/**
 * Returns the value limited to a range.
 *
 * @param min   Minimum value
 * @param max   Maximum value
 * @param value Value to be limited
 * @return Value limited in the range [min, max]
 */
inline double limit(double min, double max, double value)
{
    return std::max(min, std::min(max, value));
}
}
}

#endif
