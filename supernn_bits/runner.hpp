/*
        This file is part of SuperNN.

        SuperNN is free software: you can redistribute it and/or modify
        it under the terms of the GNU Lesser General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        SuperNN is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU Lesser General Public License for more details.

        You should have received a copy of the GNU Lesser General Public License
        along with SuperNN.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (C) 2010 - 2015 Lucas Hermann Negri
*/

#ifndef SUPERNN_RUNNER_HPP
#define SUPERNN_RUNNER_HPP

#include <vector>
#include <string>
#include "network.hpp"
#include "data.hpp"

namespace SuperNN
{
struct Bounds;

/**
 * Auxiliary class to ease the usage of an already trained neural network. It handles the
 * input, scaling, running and descaling operations.
 */
struct SUPERNN_EXPORT Runner
{
    /**
     * Constructor.
     *
     * @param net_path  Path to the neural network file
     * @param info_path Path to the scaling info file
     * @throw ErrorType if the input files couldn't be opened
     */
    Runner(const std::string &net_path, const std::string &info_path);

    Runner();
    virtual ~Runner();

    /**
     * Loads the bounds info file. Can only be called when the 'net' member variable is set.
     *
     * @param info_path Path to the bounds info file
     * @throw ErrorType if the input files couldn't be opened
     */
    void load_info_file(const std::string &info_path);

    /**
     * Sets the bounds from already built Bound objects. Can only be called when the 'net'
     * member variable is set.
     *
     * @param from From bounds
     * @param to   To bounds
     */
    void set_bounds(Bounds &from, Bounds &to);

    /**
     * Accessor to get/set an input value.
     *
     * @param idx Neuron index
     * @return Reference to the respective input value
     */
    double &inp(unsigned idx);

    /**
     * Accessor to get an output value.
     *
     * @param idx Neuron index
     * @return Value of the respective output
     */
    double out(unsigned idx);

    /**
     * Runs the network with the current input setted by inp().
     * Handles the input scaling and output descaling.
     */
    void run();

    Network net;
    Data inp_data, out_data;
};
}

#endif
