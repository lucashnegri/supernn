/*
    This file is part of SuperNN.

    SuperNN is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SuperNN is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SuperNN.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (C) 2010 - 2015 Lucas Hermann Negri
*/

#ifndef SUPERNN_ACTIVATION_HPP
#define SUPERNN_ACTIVATION_HPP

#include <cmath>
#include "neuron.hpp"
#include "utils.hpp"

namespace SuperNN
{
/**
 * Activation functions were not implemented in an OO way due to performance.
 */

/**
 * Sigmoid function. \f$ g(net) = 1 / (1 + e^{-2*s*net}) \f$
 * Span: [0, 1].
 */
struct Sigmoid
{
    static inline double activation(const Neuron &neuron)
    {
        return 1 / (1 + std::exp(-2 * neuron.steep * neuron.net));
    }

    static inline double derivative(const Neuron &neuron)
    {
        double out = Utils::limit(0.01, 0.99, neuron.out);
        return 2 * neuron.steep * out * (1 - out);
    }
};

/**
 * Sigmoid symmetric function. \f$ g(net) = 2 / (1 + e^{-2*s*net}) - 1 \f$
 * Span: [-1, 1].
 */
struct SigmoidSymmetric
{
    static inline double activation(const Neuron &neuron)
    {
        return 2 / (1 + std::exp(-2 * neuron.steep * neuron.net)) - 1.0;
    }

    static inline double derivative(const Neuron &neuron)
    {
        double out = Utils::limit(-0.98, 0.98, neuron.out);
        return neuron.steep * (1 - (out * out));
    }
};

/**
 * Elliot sigmoid-like function. \f$ g(net) = \frac{net*s/2}{1+|net*s|} + 0.5 \f$.
 * Span: [0, 1].
 */
struct Elliot
{
    static inline double activation(const Neuron &neuron)
    {
        double t = neuron.net * neuron.steep;
        return (t / 2) / (1 + std::abs(t)) + 0.5;
    }

    static inline double derivative(const Neuron &neuron)
    {
        double t = 1 + fabs(neuron.net * neuron.steep);
        return neuron.steep / (2 * t * t);
    }
};

/**
 * Elliot sigmoid-like function (Symmetric). \f$ g(net) = \frac{net*s}{1 + |net*s|} \f$.
 * Span: [-1, 1].
 */
struct ElliotSymmetric
{
    static inline double activation(const Neuron &neuron)
    {
        double t = neuron.net * neuron.steep;
        return t / (1 + std::abs(t));
    }

    static inline double derivative(const Neuron &neuron)
    {
        double t = 1 + fabs(neuron.net * neuron.steep);
        return neuron.steep / (t * t);
    }
};

/**
 * Gaussian function. \f$ g(net) = e^{-(net*s)^2} \f$.
 * Span: [0, 1].
 */
struct Gaussian
{
    static inline double activation(const Neuron &neuron)
    {
        double a = neuron.net * neuron.steep;
        return std::exp(-a * a);
    }

    static inline double derivative(const Neuron &neuron)
    {
        return -2 * neuron.net * neuron.out * neuron.steep * neuron.steep;
    }
};

/**
 * Gaussian symmetric function. \f$ g(net) = 2e^{-(net*s)^2)} -1 \f$.
 * Span: [-1, 1].
 */
struct GaussianSymmetric
{
    static inline double activation(const Neuron &neuron)
    {
        double a = neuron.net * neuron.steep;
        return 2 * exp(-a * a) - 1;
    }

    static inline double derivative(const Neuron &neuron)
    {
        return -2 * neuron.net * (neuron.out + 1) * neuron.steep * neuron.steep;
    }
};

/**
 * Linear function. \f$ g(net) = s * net \f$.
 * Span: [\f$\infty_{-}, \infty_{+}\f$].
 */
struct Linear
{
    static inline double activation(const Neuron &neuron)
    {
        return neuron.steep * neuron.net;
    }

    static inline double derivative(const Neuron &neuron)
    {
        return neuron.steep;
    }
};

/**
 * Sign function (net >= 0 ? 1 : -1).
 * Span: [-1, 1].
 */
struct Sign
{
    static inline double activation(const Neuron &neuron)
    {
        return neuron.net >= 0 ? 1 : -1;
    }

    static inline double derivative(const Neuron &neuron)
    {
        (void)neuron;
        throw Exception(ERROR_NOT_DIFERENTIABLE);
    }
};

/**
 * Leaky rectified linear unit (LReLU) function (net > 0 ? net : 0.01net).
 * Span: [-inf, inf].
 */
struct LReLU
{
    static inline double activation(const Neuron &neuron)
    {
        const double v = neuron.steep * neuron.net;
        return neuron.net >= 0 ? v : 0.01 * v;
    }

    static inline double derivative(const Neuron &neuron)
    {
        return neuron.net >= 0 ? neuron.steep : 0.01 * neuron.steep;
    }
};

/** Activation function dispatcher */
struct ActFunc
{
    /**
     * Calls the actual activation function.
     *
     * @param neuron Neuron to calculate the output of the activation function
     * @return Activation output
     */
    static inline double activation(const Neuron &neuron)
    {
        switch(neuron.act_func)
        {
            case ACT_SIGMOID:
                return Sigmoid::activation(neuron);

            case ACT_SIGMOID_SYMMETRIC:
                return SigmoidSymmetric::activation(neuron);

            case ACT_LINEAR:
                return Linear::activation(neuron);

            case ACT_ELLIOT:
                return Elliot::activation(neuron);

            case ACT_ELLIOT_SYMMETRIC:
                return ElliotSymmetric::activation(neuron);

            case ACT_GAUSSIAN:
                return Gaussian::activation(neuron);

            case ACT_GAUSSIAN_SYMMETRIC:
                return GaussianSymmetric::activation(neuron);

            case ACT_SIGN:
                return Sign::activation(neuron);

            case ACT_LRELU:
                return LReLU::activation(neuron);

            case ACT_LAST:
                return 0.0; /* should not be reached */
        }

        /* should not be reached */
        return 0.0;
    }

    /**
     * Calls the actual derivative of the activation function, used to calculate the
     * error gradient.
     *
     * @param neuron Neuron to calculate the derivative of the activation function
     * @return Activation derivative
     */
    static inline double derivative(const Neuron &neuron)
    {
        switch(neuron.act_func)
        {
            case ACT_SIGMOID:
                return Sigmoid::derivative(neuron);

            case ACT_SIGMOID_SYMMETRIC:
                return SigmoidSymmetric::derivative(neuron);

            case ACT_LINEAR:
                return Linear::derivative(neuron);

            case ACT_ELLIOT:
                return Elliot::derivative(neuron);

            case ACT_ELLIOT_SYMMETRIC:
                return ElliotSymmetric::derivative(neuron);

            case ACT_GAUSSIAN:
                return Gaussian::derivative(neuron);

            case ACT_GAUSSIAN_SYMMETRIC:
                return GaussianSymmetric::derivative(neuron);
                
            case ACT_LRELU:
                return LReLU::derivative(neuron);

            case ACT_SIGN:
                return Sign::derivative(neuron);

            case ACT_LAST:
                return 0.0; /* should not be reached */
        }

        /* should not be reached */
        return 0.0;
    }
};

}

#endif
