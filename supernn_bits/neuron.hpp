/*
        This file is part of SuperNN.

        SuperNN is free software: you can redistribute it and/or modify
        it under the terms of the GNU Lesser General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        SuperNN is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU Lesser General Public License for more details.

        You should have received a copy of the GNU Lesser General Public License
        along with SuperNN.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (C) 2010 - 2015 Lucas Hermann Negri
*/

#ifndef SUPERNN_NEURON_HPP
#define SUPERNN_NEURON_HPP

#include <vector>
#include "activation_type.hpp"
#include "utils.hpp"

namespace SuperNN
{
/**
 * Synaptic connection between two neurons.
 */
struct SUPERNN_EXPORT Connection
{
    Connection();
    virtual ~Connection();

    /**
     * Constructor.
     *
     * @param _l Layer where the target neuron is located
     * @param _n Position of the target neuron in it's layer
     * @param _w Connection weight
     *
     * @return Connection instance
     */
    Connection(unsigned _l, unsigned _n, double _w = (Utils::rand_double(0.5) - 0.25));

    /** Weight */
    double weight;

    /** Accumulated partial error derivative in respect to the connection weight */
    double derror;

    /** Auxiliary storage 1, used by some training algorithms */
    double aux1;

    /** Auxiliary storage 2, used by some training algorithms */
    double aux2;

    /** Layer where the target neuron is located */
    unsigned to_layer;

    /** Position of the target neuron in it's layer */
    unsigned to_neuron;
};

/**
 * Neuron, that can contain connections to neurons in the next layers.
 */
struct SUPERNN_EXPORT Neuron
{
    /**
     * Constructor. Bias neurons (acts like an input neuron with fixed input of 1)
     * should be placed in the input layer. There's no need for more than one
     * bias neuron, as neurons can be connected to any neuron in any next layer.
     *
     * @param _b Marks if the neuron is a bias neuron
     * @return Neuron instance
     */
    Neuron(bool _b = false);

    virtual ~Neuron();

    /**
     * Returns a const reference to a connection.
     *
     * @param c Connection position
     * @return Const reference to the connection
     */
    inline const Connection &operator[](unsigned c) const
    {
        return conns[c];
    }

    /**
     * Returns a reference to a connection.
     *
     * @param c Connection position
     * @return Reference to the connection
     */
    inline Connection &operator[](unsigned c)
    {
        return conns[c];
    }

    /**
     * Returns the number of synaptic connections.
     *
     * @return Number of synaptic connections
     */
    inline unsigned size() const
    {
        return conns.size();
    }

    /**
     * Adds a connection to a neuron.
     *
     * @param to_layer  Layer where the target neuron is located
     * @param to_neuron Position of the target neuron in it's layer
     */
    void connect(unsigned to_layer, unsigned to_neuron);

    /**
     * Sets the neuron activation function.
     *
     * @param type Activation function type
     * @param s    Activation function steepness
     */
    void set_activation(ActFuncType type, double s = 1);

    /** Synaptic connections */
    std::vector<Connection> conns;

    /** Last sum of the neuron inputs */
    double net;

    /** Last output of the neuron ( g(net) ) */
    double out;

    /** Last error (desired - actual). Only for output neurons */
    double err;

    /** Last local error gradient */
    double delta;

    /** Used activation function */
    ActFuncType act_func;

    /** Activation function steepness */
    double steep;

    /** Marks if the delta has been calculated for the current iteration */
    bool delta_ok;

    /** Marks if it's a bias neuron */
    bool bias;
};
}

#endif
