/*
    This file is part of SuperNN.

    SuperNN is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SuperNN is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SuperNN.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (C) 2010 - 2015 Lucas Hermann Negri
*/

#ifndef SUPERNN_TRAINING_HPP
#define SUPERNN_TRAINING_HPP

#include "utils.hpp"

namespace SuperNN
{
struct Data;
struct Network;

/**
 * Abstract class that provides the calculation of the error derivatives and
 * the error accumulation, used by the derived backpropagation training algorithms.
 */
struct SUPERNN_EXPORT TrainingAlgorithm
{
    virtual ~TrainingAlgorithm();

    /**
     * Prepares the trainer and a neural network for training. Usually
     * this will initialize the learning rates, previous error
     * informations and prepare the internal data structures for the
     * training. Between a prepare() and a train(), the neural network and
     * the trainer shouldn't be modified.
     *
     * @param net Neural network to be prepared
     */
    virtual void prepare(Network &net);

    /**
     * Adjusts the synaptic weight of an artificial neural network in order to minimize
     * the error (MSE for standard l2 training or MAE for specific l1 training).
     *
     * @param net        Artificial neural network to be trained
     * @param data       Training data
     * @param dmse       Desired mean squared error (or MAE when applied). Stopping condition
     * @param max_epochs Maximum number of epochs to train the network. Stopping condition
     * @return The number of training epochs performed
     */
    virtual unsigned train(Network &net, const Data &data, double dmse = 0,
                           unsigned max_epochs = 1000) = 0;

    /**
     * Calculates the local error gradient for each neuron.
     * It's a recursive operation, where calculating the delta for
     * a neuron will automatically calculate the delta for the neurons in the
     * next layers that have connections with this neuron.
     *
     * For an output neuron j:
     * \f$\delta_{j} = err * g'(net_j)\f$
     *
     * For a hidden neuron j:
     * \f$\delta_{j} = \displaystyle\sum_{k} delta_{k} * w_{jk} * g'(net_j) \f$
     *
     * @param net Neural network to be used
     * @param l   Layer where the neuron is located
     * @param n   Neuron position in the layer
     * @return Calculated delta
     */
    virtual double delta(Network &net, unsigned l, unsigned n);

    /**
     * Accumulates the error partial derivative in respect to the weights, for
     * each connection of the neural network.
     *
     * \f$\frac{\partial E}{\partial w_{ij}} = -\delta_j * y_i\f$
     *
     * @param net Neural network
     */
    virtual void derror_acc(Network &net);

    /**
     * Clears the accumulated error partial derivatives.
     *
     * @param net Neural network
     */
    virtual void clear_derror_acc(Network &net);

protected:
    /**
     * Checks if the dimensions match and if the training algorithm can be used with
     * a given network and data.
     *
     * @param net   Neural network to be trained
     * @param data  Training data
     * @throw Exception if the algorithm can not be used with the network or data
     */
    virtual void check(const Network& net, const Data& data) const;
};

/**
 * Base class for the standard backpropagation algorithm.
 */
struct SUPERNN_EXPORT ImplBackprop : public TrainingAlgorithm
{
    ImplBackprop();
    virtual ~ImplBackprop();

    virtual unsigned train(Network &net, const Data &data, double dmse, unsigned max_epochs) = 0;

    /** Learning rate decrease factor (must be <= 1) */
    double eta_df;

    /** Learning rate increase factor (must be >= 1) */
    double eta_if;

    /** Minimum learning rate */
    double eta_min;

    /** Maximum learning rate */
    double eta_max;

    /** Initial learning rate */
    double eta;

protected:
    /**
     * Updates the weights using the accumulated error partial derivative
     * calculated by derror_acc().
     *
     * @param net    Neural network
     * @param factor Factor that will be multiplied with the learning rate
     * to adjust the weights
     */
    virtual void update_weights(Network &net, double factor = 1);

    /**
     * Calculates the new learning rate (and updates it), based on the mean
     * squared error last change.
     *
     * @param mse       Current mean squared error
     * @param last_mse  Last mean squared error
     */
    virtual void update_eta(double mse, double last_mse);
};

/**
 * Incremental backpropagation. The weights are changed after each presented
 * training pattern.
 */
struct SUPERNN_EXPORT Incremental : public ImplBackprop
{
    Incremental();
    virtual ~Incremental();
    virtual unsigned train(Network &net, const Data &data, double dmse = 0, unsigned max_epochs = 1000);
};

/**
 * Batch backpropagation. The weights are changed only after all training patterns
 * have been presented (in the end of each epoch).
 */
struct SUPERNN_EXPORT Batch : public ImplBackprop
{
    Batch();
    virtual ~Batch();
    virtual unsigned train(Network &net, const Data &data, double dmse = 0, unsigned max_epochs = 1000);
};

/**
 * Improved resilient backpropagation algorithm. Uses a dedicated, self-adjusting
 * learning rate for each connection, using only the sign change of the partial
 * error derivatives, not the amplitude, to guide the weight optimization.
 * For more information, see [Igel and Hüsken, 2000].
 */
struct SUPERNN_EXPORT IRprop : public TrainingAlgorithm
{
    IRprop();
    virtual ~IRprop();

    virtual void prepare(Network &net);
    virtual unsigned train(Network &net, const Data &data, double dmse = 0, unsigned max_epochs = 1000);

    /** Weight change decrease factor. Must be <= 1 */
    double delta_df;

    /** Weight change increase factor. Must be >= 1 */
    double delta_if;

    /** Minimum weight change */
    double delta_min;

    /** Maximum weight change */
    double delta_max;

    /** Initial weight change */
    double delta_zero;

protected:
    /**
     * Updates the weights, using the partial error derivative sign change as guide.
     *
     * @param net Neural network
     */
    virtual void update_weights(Network &net);
};

/**
 * Modified improved resilient backpropagation algorithm. Minimizes the absolute error instead
 * of the squared error.
 */
struct SUPERNN_EXPORT IRpropL1 : public IRprop
{
public:
    IRpropL1();
    virtual ~IRpropL1();
    double delta(Network &net, unsigned l, unsigned n);
};

/**
 * Neuron by Neuron algorithm. Consists in the application of the Levenberg–Marquardt
 * optimization algorithm, but building the Quasi-Hessian matrix directly, without the
 * need for building the Jacobian matrix, thus saving memory.
 * For more information, see [Wilamowski, Yu, 2010].
 */
struct SUPERNN_EXPORT NBN : public TrainingAlgorithm
{
    NBN();
    virtual ~NBN();

    void prepare(Network &net);
    unsigned train(Network &net, const Data &data, double dmse = 0, unsigned max_epochs = 100);

private:
    /**
     * Like regular delta, but uses the derivative of the error instead of the
     * quadratic error, since the derivative of the quadratic error is estimated on the
     * quasi-Hessian computation. Also calculates the delta for each neuron separately.
     *
     * @param net Neural network to be used
     * @param l   Layer where the neuron is located
     * @param n   Neuron position in the layer
     * @param m   Output neuron to calculate the error derivative
     * @return Calculated delta
     */
    double delta(Network &net, unsigned l, unsigned n, unsigned m);

    /**
     * Copies the weights of a Network into the weight matrix.
     *
     * @param net Network to copy the weights from
     */
    void get_weights(const Network &net);

    /**
     * Sets the weights of a Network from the weight matrix.
     *
     * @param net Network to set the weights
     */
    void set_weights(Network &net) const;

    /**
     * Calculates a line of the jacobian matrix, transposed.
     *
     * @param net Network to calculate the error derivatives
     * @paran out Output neuron to calculate the error derivatives for
     */
    void calc_jacobian_transp_line(Network &net, unsigned m);

    /**
     * Updates the Hessian and the error gradient matrices using the
     * current Jacobian matrix line.
     *
     * @param err Error related to the current pattern/output neuron
     */
    void update_hessian_gradient(double err);

    /**
     * Updates the weights of the network based on the current Hessian and
     * error gradient matrices.
     */
    void update_weights(Network &net);

    /**
     * Updates the value of mu, based on the error progression. Also, if
     * the error raised, backtracks to the previous weights.
     *
     * @param mse       Current mean squared error
     * @param last_mse  Last mean squared error
     * @return If the last weight update reduced the mse
     */
    bool update_mu(double mse, double last_mse);

    /**
     * Initial value of the balance of thre Gauss-Newton and the
     * gradient descent directions.
     */
    double mu_zero;

    /**
     * Current value of mu (see mu_zero).
     */
    double mu;

    /**
     * Minimum value of mu (see mu_zero).
     */
    double mu_min;

    /**
     * Maximum value of mu (see mu_zero).
     */
    double mu_max;

    /**
     * Factor used to change the value of mu (see mu_zero).
     */
    double beta;

    /**
     * Number of local iterations (weight and mu updates) to perform at each epoch.
     */
    unsigned local_iters;

    /**
     * Total number of weights (equal to the number of connections) of
     * the network.
     */
    unsigned n_weights;

private:
    struct Hist;
    Hist *h; // private data members
};

/* higher level training helpers */

/**
 * Estimates the performance of a neural network for an independent data set by using k-fold cross validation.
 *
 * @param train      Training algorithm to be evaluated
 * @param net        Artificial neural network topology to be trained
 * @param data       Data to be used in the training and validation
 * @param k          Number of partitions to use in the k-fold procedure
 * @param dmse       Desired MSE (stopping condition)
 * @param max_epochs Max training epochs (stopping condition)
 */
SUPERNN_EXPORT double k_fold_error(TrainingAlgorithm &algo, const SuperNN::Network &net, const Data& data,
                    unsigned k = 10, double dmse = 0, unsigned max_epochs = 1000);

/**
 * Trains an artificial neural network by using early stopping in order to avoid over-fitting.
 * The training is performed with the training set until the error on the validation set rises,
 * indicating an overfitting.
 *
 * To reduce the influence of local minima, the training continues for at least max_stuck iterations.
 * The overfitting is verified every step_size steps.
 *
 * @param train         Training algorithm to be evaluated
 * @param net           Artificial neural network topology to be trained
 * @param training      Training data
 * @param validation    Validation data
 * @param step_size     Number of epochs to train at each verification
 * @param max_stuck     Maximum number of epochs to test for local minima
 * @return Number of epochs when the training stopped (before overfitting)
 */
SUPERNN_EXPORT unsigned early_stopping(TrainingAlgorithm& algo, Network& net, const Data& training,
                        const SuperNN::Data &validation, unsigned step_size = 1, unsigned max_stuck = 20, unsigned max_epochs = 1000);

}

#endif
