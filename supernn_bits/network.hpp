/*
        This file is part of SuperNN.

        SuperNN is free software: you can redistribute it and/or modify
        it under the terms of the GNU Lesser General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        SuperNN is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU Lesser General Public License for more details.

        You should have received a copy of the GNU Lesser General Public License
        along with SuperNN.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (C) 2010 - 2015 Lucas Hermann Negri
*/

#ifndef SUPERNN_NETWORK_HPP
#define SUPERNN_NETWORK_HPP

#include <vector>

#include "data.hpp"
#include "activation_type.hpp"

namespace SuperNN
{
struct Neuron;

/**
 * Array of neurons.
 * Neurons are only permitted to have connections to latter layers.
 */
struct SUPERNN_EXPORT Layer : public std::vector<Neuron>
{
    Layer();
    virtual ~Layer();

    /**
     * Adds a neuron to the layer.
     *
     * @param n Neuron to be added
     */
    void add_neuron(Neuron &n);

    /**
     * Adds a number of neurons to the layer.
     *
     * @param n_neurons Number of neurons to add
     * @param bias True to add bias neurons, false otherwise
     */
    void add_neurons(unsigned n_neurons, bool bias = false);

    /**
     * Sets the activation function for all the neurons currently
     * in the layer.
     *
     * @param type Activation function type
     * @param s    Activation function steepness
     */
    void set_activation(ActFuncType type, double s = 1);

    /**
     * Connects all the neurons of the layer to a neuron.
     *
     * @param to_layer  Layer where the target neuron is located
     * @param to_neuron Position of the target neuron in it's layer
     */
    void connect(unsigned to_layer, unsigned to_neuron);
};

/**
 * Artificial neural network structure that supports arbitrary feedforward topologies, like
 * multilayer perceptrons and fully connected cascade networks.
 */
struct SUPERNN_EXPORT Network
{
    /**
     * Constructs a 'standard' feed forward neural network with one
     * hidden layer.
     *
     * @param input  Number of neurons in the input layer
     * @param hidden Number of neurons in the hidden layer
     * @param output Number of neurons in the output layer
     * @return Network instance
     */
    static Network make_mlp(unsigned input, unsigned hidden, unsigned output);

    /**
     * Constructs a 'standard' feed forward neural network without
     * hidden layers.
     *
     * @param input  Number of neurons in the input layer
     * @param output Number of neurons in the output layer
     * @return Network instance
     */
    static Network make_mlp(unsigned input, unsigned output);

    /**
     * Constructs a 'standard' feed forward neural network with one
     * hidden layer.
     *
     * @param input   Number of neurons in the input layer
     * @param hidden1 Number of neurons in the first hidden layer
     * @param hidden2 Number of neurons in the second hidden layer
     * @param output  Number of neurons in the output layer
     * @return Network instance
     */
    static Network make_mlp(unsigned input, unsigned hidden1, unsigned hidden2, unsigned output);

    /**
     * Constructs a fully connected cascade neural network.
     *
     * @param input  Number of neurons in the input layer
     * @param hidden Number of hidden layers
     * @param output Number of neurons in the output layer
     * @return Network instance
     */
    static Network make_fcc(unsigned input, unsigned hidden, unsigned output);

    Network();
    virtual ~Network();

    /**
     * Adds a layer to the network.
     *
     * @param l Layer to be added
     */
    void add_layer(Layer &l);

    /**
     * Adds a number of layers to the network.
     *
     * @param n_layers Number of layers to be added
     */
    void add_layers(unsigned n_layers);

    /**
     * Propagates an input in the network. Note that this overwrites informations
     * about the last run.
     *
     * @param in         Data row to run the network with
     * @param calc_error If the output error and the MSE must be updated on the run
     * @return Reference to the output
     */
    const Row &run(const Row &in, bool calc_error = false);

    /**
     * Calculates the mean squared error of the network related to a data.
     * As it calls run(), it also erases the information about the last run.
     *
     * @param data Data used to evaluate the mean squared error
     * @return Mean squared error of the network related to the used data
     */
    double calc_mse(const Data &data);

    /**
     * Calculates the mean absolute error of the network related to a data.
     * As it calls run(), it also erases the information about the last run.
     *
     * @param data Data used to evaluate the mean absolute error
     * @return Mean absolute error of the network related to the used data
     */
    double calc_mae(const Data &data);
    
    /**
     * Calculates the classification rate of the network related to a data.
     * Verifies if every output differ less than the limit with the expected
     * value.
     *
     * @param data Data used in the evaluation
     * @param limit Limit between a correct classification and an incorrect one
     * @return Classification rate, in the range [0, 100]
     */
    double calc_class(const Data &data, double limit = 0.5);

    /**
     * Calculates the classification rate of the network related to a data.
     * Only shows usefull information for 'one of c-classes' codifications.
     * Searches for the higher output and expected value and checks if the
     * neuron match.
     *
     * @param data Data used in the evaluation
     * @return Classification rate, in the range [0, 100]
     */
    double calc_class_higher(const Data &data);

    /**
     * Initializes the weights with pseudo-ramdom numbers.
     *
     * @param min Minimum weight
     * @param max Maximum weight
     */
    void init_weights(double min = -0.5, double max = 0.5);

    /**
     * Sets the activation function for all the neurons currently
     * in the network.
     *
     * @param type Activation function type
     * @param s    Activation function steepness
     */
    void set_activation(ActFuncType type, double s = 1);

    /**
     * Saves the network contents to a file, for latter use.
     *
     * @param path Output file path
     * @throw Exception if the output file couldn't be written
     */
    void save_file(const std::string &path) const;

    /**
     * Loads the network contents from a file.
     *
     * @param path Input file path
     * @throw Exception if the input file couldn't be opened
     */
    void load_file(const std::string &path);

    /**
     * Connects all the neurons from a layer to all the neurons of
     * another layer.
     *
     * @param from_layer First layer
     * @param to_layer   Second layer
     */
    void connect(unsigned from_layer, unsigned to_layer);

    /**
     * Connects a neuron to all the neurons of another layer.
     *
     * @param from_layer    From layer id
     * @param from_neuron   Neuron id
     * @param to_layer      Target layer id
     */
    void connect_neuron_to_layer(unsigned from_layer, unsigned from_neuron, unsigned to_layer);

    /**
     * Clears the neuron state.
     *
     * @param clear_delta If it should clear the calculated delta
     * @param clear_run   If it should clear the last run info
     */
    void clear_neurons(bool clear_delta, bool clear_run);

    /**
     * Calculates the current number of weights.
     *
     * @return Number of weights
     */
    unsigned calc_num_weights() const;

    /**
     * Calculates the current number of neurons.
     *
     * @return Number of neurons
     */
    unsigned calc_num_neurons() const;

    /**
     * Calculates the number of neurons on the first layer that aren't biases.
     */
    unsigned calc_num_inputs() const;
    
    /**
     * Returns a const reference to a layer.
     *
     * @param l Layer position
     * @return Const reference to the layer
     */
    const Layer &operator[](unsigned l) const;

    /**
     * Returns a reference to a layer.
     *
     * @param l Layer position
     * @return Reference to the layer
     */
    Layer &operator[](unsigned l);

    /**
     * Returns the number of layers.
     *
     * @return Number of layers
     */
    unsigned size() const;

    /** Neuron layers. */
    std::vector<Layer> layers;

    /** Structure that holds the last output values. */
    Row last_output;

protected:    
    /**
     * Last computed number of neurons in the input layer that aren't biases, computed
     * by run(). For internal use. The user should refer to calc_num_inputs, that is a pure
     * function.
     */
    unsigned n_input;
};
}

#endif
