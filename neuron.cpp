/*
        This file is part of SuperNN.

        SuperNN is free software: you can redistribute it and/or modify
        it under the terms of the GNU Lesser General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        SuperNN is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU Lesser General Public License for more details.

        You should have received a copy of the GNU Lesser General Public License
        along with SuperNN.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (C) 2010 - 2015 Lucas Hermann Negri
*/

#include "neuron.hpp"

namespace SuperNN
{
Connection::Connection()
{
}

Connection::~Connection()
{
}

Connection::Connection(unsigned _l, unsigned _n, double _w)
    : weight(_w), to_layer(_l), to_neuron(_n)
{
}

Neuron::~Neuron()
{
}

Neuron::Neuron(bool _b) : net(0.0), out(0.0), err(0.0), delta(0.0), act_func(ACT_SIGMOID_SYMMETRIC),
    steep(1.0), delta_ok(false), bias(_b)
{
}

void Neuron::connect(unsigned to_layer, unsigned to_neuron)
{
    conns.push_back(Connection(to_layer, to_neuron));
}

void Neuron::set_activation(ActFuncType type, double s)
{
    act_func = type;
    steep = s;
}

}
